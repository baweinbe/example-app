const fastify = require('fastify')();
const { v4: uuidv4 } = require('uuid');
const cors = require('@fastify/cors');

fastify.register(cors);

const todos = [];

async function sleep() {
    await new Promise((res) => setTimeout(res, 2000));
}

fastify.get('/todos', async (request, reply) => {
    await sleep();
    return todos;
});

fastify.post('/todos', async (request, reply) => {
    const { text } = request.body;
    await sleep();
    if (text === 'err') {
        return reply.status(500).send();
    }
    const newTodo = { id: uuidv4(), text, completed: false };
    todos.push(newTodo);
    return newTodo;
});

fastify.put('/todos/:id', async (request, reply) => {
    await sleep();
    const { id } = request.params;
    const { completed } = request.body;
    const todo = todos.find((todo) => todo.id === id);
    if (todo) {
        todo.completed = completed;
        return todo;
    }
    reply.status(404).send();
});

fastify.delete('/todos/:id', async (request, reply) => {
    await sleep();
    const { id } = request.params;
    const index = todos.findIndex((todo) => todo.id === id);
    if (index !== -1) {
        todos.splice(index, 1);
        reply.send();
    } else {
        reply.status(404).send();
    }
});

const start = async () => {
    try {
        await fastify.listen({ port: 5000 })
        console.log(`server listening on ${fastify.server.address().port}`)
    } catch (err) {
        console.error(err)
        process.exit(1)
    }
}
start()
