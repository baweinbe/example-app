import React, { useContext } from 'react';
import { useAppSelector } from '../common/app/constants/store';
import { selectTodos } from '../todos-redux/reducers/todos.reducer';
import { TodosStoreContext } from '../todos-mobx/stores/TodosStore';

export function Faq() {
  const todos = useAppSelector(selectTodos);
  const todosMobx = useContext(TodosStoreContext);

  return (
    <div>
      <p>Redux todo list:</p>
      <ul>
        {todos.map(todo => (
          <li key={todo.id}>{todo.text}</li>
        ))}
      </ul>
      <p>Mobx todo list:</p>
      <ul>
        {todosMobx.todos.map(todo => (
          <li key={todo.id}>{todo.text}</li>
        ))}
      </ul>
    </div>
  );
}
