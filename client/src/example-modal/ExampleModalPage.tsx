import { useModal } from '../common/app/hooks/use-modal';
import { Button } from '@mui/material';
import React from 'react';
import { ExampleModal } from '../example-modal/ExampleModal';
import { MODAL_VALUE_FAQ } from '../common/app/constants/query-params';

export function ExampleModalPage() {
  const { isOpen, onOpen, onClose } = useModal(MODAL_VALUE_FAQ);
  return (
    <div>
      <Button onClick={onOpen}>Open Modal</Button>
      <ExampleModal open={isOpen} onClose={onClose} />
    </div>
  );
}
