import React, { useState } from 'react';
import { TodosStore } from '../stores/TodosStore';
import { observer } from 'mobx-react-lite';

export const AddTodoForm = observer(({ todosStore }: { todosStore: TodosStore }) => {
  const [text, setText] = useState('');

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    todosStore.addTodo(text);
    setText('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" value={text} onChange={event => setText(event.target.value)} />
      <button type="submit">Add Todo</button>
    </form>
  );
});
