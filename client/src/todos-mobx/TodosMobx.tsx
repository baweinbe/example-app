import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { TodosStoreContext } from './stores/TodosStore';
import { AddTodoForm } from './components/AddTodo';

export const TodosMobx = observer(() => {
  const todosStore = useContext(TodosStoreContext);

  return (
    <div>
      <AddTodoForm todosStore={todosStore} />
      <ul>
        {todosStore.todos.map((todo, i) => (
          <li key={todo.id}>
            <input type="checkbox" checked={todo.completed} onChange={() => todosStore.toggleTodo(i)} />
            {todo.text}
            <button onClick={() => todosStore.deleteTodo(i)}>Delete</button>
          </li>
        ))}
      </ul>
    </div>
  );
});
