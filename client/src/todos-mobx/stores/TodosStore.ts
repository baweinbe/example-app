import { createContext } from 'react';
import { makeAutoObservable } from 'mobx';

type Todo = {
  id: number;
  text: string;
  completed: boolean;
};

export class TodosStore {
  todos: Todo[] = [];

  constructor() {
    makeAutoObservable(this);
  }

  addTodo(text: string) {
    this.todos.push({ id: Date.now(), text, completed: false });
  }

  toggleTodo(index: number) {
    this.todos[index].completed = !this.todos[index].completed;
  }

  deleteTodo(index: number) {
    this.todos.splice(index, 1);
  }
}

export const TodosStoreContext = createContext<TodosStore>(new TodosStore());
