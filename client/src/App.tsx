import React from 'react';
import { QueryClientProvider } from '@tanstack/react-query';
import { RouterProvider } from 'react-router-dom';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { router } from './common/app/constants/router';
import { apiClient } from './common/app/constants/api-client';
import { store } from './common/app/constants/store';
import { Provider } from 'react-redux';
import { TodosStore, TodosStoreContext } from './todos-mobx/stores/TodosStore';

function App() {
  return (
    <QueryClientProvider client={apiClient}>
      <ReactQueryDevtools initialIsOpen={false} />
      <Provider store={store}>
        <TodosStoreContext.Provider value={new TodosStore()}>
          <RouterProvider router={router} />
        </TodosStoreContext.Provider>
      </Provider>
    </QueryClientProvider>
  );
}

export default App;
