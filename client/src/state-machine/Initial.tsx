import {useDispatch} from "react-redux";
import React from "react";
import {useAppSelector} from "../common/app/constants/store";
import {selectMachine, updateText} from "./reducers/state-machine.reducer";
import {useNavigate} from "react-router-dom";
import {STATE_MACHINE_CHILD_PATH} from "../common/app/constants/paths";

export const Initial = () => {
    const { text } = useAppSelector(selectMachine);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const onChange = (event: any) => {
        dispatch(updateText(event.target.value));
    };

    const onSubmit = () => {
        navigate(STATE_MACHINE_CHILD_PATH);
    };

    return (
        <form onSubmit={onSubmit}>
            <input type="text" value={text} onChange={onChange} />
            <button type="submit">Submit</button>
        </form>
    );
};