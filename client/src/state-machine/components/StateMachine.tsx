import React from 'react';
import {selectMachine, State, updateState} from "../reducers/state-machine.reducer";
import {useAppSelector} from "../../common/app/constants/store";
import {Button} from "@mui/material";
import {useDispatch} from "react-redux";
import {ReactJSXElement} from "@emotion/react/types/jsx-namespace";

const First = () => {
    const { text } = useAppSelector(selectMachine);
    const dispatch = useDispatch();
    return (
        <div>
            <p>This is what you wrote: {text}</p>
            Only one place to go from here: <Button onClick={() => dispatch(updateState('second'))}>Next</Button>
        </div>
    )
};

const Second = () => {
    const dispatch = useDispatch();
    return (
        <div>
            <Button onClick={() => dispatch(updateState('first'))}>Back</Button>
            <Button onClick={() => dispatch(updateState('third'))}>Next</Button>
        </div>
    )
};

const Third = () => {
    const dispatch = useDispatch();
    return (
        <div>
            <Button onClick={() => dispatch(updateState('second'))}>Back</Button>
        </div>
    )
};

const stateComponents: Record<State, ReactJSXElement> = {
    first: <First />,
    second: <Second />,
    third: <Third />,
};

export const StateMachine = () => {
    const { state } = useAppSelector(selectMachine);
    return stateComponents[state];
};