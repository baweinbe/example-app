import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../common/app/constants/store';

export type State = 'first' | 'second' | 'third';
type MachineState = {
  state: State;
  text: string;
};

const initialState: MachineState = {
  state: 'first',
  text: '',
};

export const todoSlice = createSlice({
  name: 'machine',
  initialState,
  reducers: {
    updateState: (state, action: PayloadAction<State>) => {
      state.state = action.payload;
    },
    updateText: (state, action: PayloadAction<string>) => {
      state.text = action.payload;
    },
  },
});

export const selectMachine = (state: RootState) => state.machine;
export const { updateState, updateText } = todoSlice.actions;
export const machineReducer = todoSlice.reducer;
