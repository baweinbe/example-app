import React from 'react';
import { getGetTodosQueryKey, useUpdateTodo } from '../../generated/api';
import { updateListItem, useOptimisticOptions } from '../../common/app/utils/optimistic-rendering';
import { Todo } from '../../generated/api.schemas';

type CompleteTodoServerProps = {
  todo: Todo;
};
export const CompleteTodo = ({ todo }: CompleteTodoServerProps) => {
  const options = useOptimisticOptions({
    queryKey: getGetTodosQueryKey(),
    createOptimisticData: updateListItem(todo.id),
  });
  const { mutate } = useUpdateTodo({
    mutation: options,
  });

  return (
    <input
      type="checkbox"
      checked={todo.completed}
      onChange={() => mutate({ id: todo.id, data: { completed: !todo.completed } })}
    />
  );
};
