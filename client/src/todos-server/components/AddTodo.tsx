import React, { useState } from 'react';
import { getGetTodosQueryKey, useCreateTodo } from '../../generated/api';
import { createListItem, useOptimisticOptions } from '../../common/app/utils/optimistic-rendering';

export const AddTodo = () => {
  const [text, setText] = useState('');

  const options = useOptimisticOptions({
    queryKey: getGetTodosQueryKey(),
    createOptimisticData: createListItem(),
  });
  const { mutate } = useCreateTodo({
    mutation: options,
  });

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    mutate({
      data: { text },
    });
    setText('');
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <input type="text" value={text} onChange={event => setText(event.target.value)} />
        <button type="submit">Add Todo</button>
      </form>
    </>
  );
};
