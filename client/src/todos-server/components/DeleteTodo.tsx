import React from 'react';
import { getGetTodosQueryKey, useDeleteTodo } from '../../generated/api';
import { deleteListItem, isOptimisticId, useOptimisticOptions } from '../../common/app/utils/optimistic-rendering';
import { Todo } from '../../generated/api.schemas';

type DeleteTodoServerProps = {
  todo: Todo;
};
export const DeleteTodo = ({ todo }: DeleteTodoServerProps) => {
  const options = useOptimisticOptions({
    queryKey: getGetTodosQueryKey(),
    createOptimisticData: deleteListItem(todo.id),
  });
  const { mutate } = useDeleteTodo({
    mutation: options,
  });

  if (isOptimisticId(todo.id)) {
    return null;
  }

  return <button onClick={() => mutate({ id: todo.id })}>Delete</button>;
};
