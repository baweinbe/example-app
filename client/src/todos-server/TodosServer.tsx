import React from 'react';
import { useGetTodos } from '../generated/api';
import { QueryGuard } from '../common/app/components/QueryGuard';
import { DeleteTodo } from './components/DeleteTodo';
import { AddTodo } from './components/AddTodo';
import { CompleteTodo } from './components/CompleteTodo';

export const TodosServer = () => {
  const { data, isLoading } = useGetTodos();

  return (
    <>
      <AddTodo />
      <QueryGuard isLoading={isLoading}>
        <ul>
          {data?.map(todo => (
            <li key={todo.id}>
              <CompleteTodo todo={todo} />
              {todo.text}
              <DeleteTodo todo={todo} />
            </li>
          ))}
        </ul>
      </QueryGuard>
    </>
  );
};
