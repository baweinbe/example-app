import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../common/app/constants/store';

type NestedState = {
  isOn: boolean;
};

const initialState: NestedState = {
  isOn: false,
};

export const nestedSlice = createSlice({
  name: 'nested',
  initialState,
  reducers: {
    toggle: state => {
      state.isOn = !state.isOn;
    },
  },
});

export const selectNested = (state: RootState) => state.nested;
export const { toggle } = nestedSlice.actions;
export const nestedReducer = nestedSlice.reducer;
