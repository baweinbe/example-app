import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Todo } from '../types/todos.types';
import { RootState } from '../../common/app/constants/store';

type TodoState = {
  todos: Todo[];
};

const initialState: TodoState = {
  todos: [],
};

export const todoSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<Todo>) => {
      state.todos.push(action.payload);
    },
    toggleTodo: (state, action: PayloadAction<number>) => {
      const todo = state.todos.find(todo => todo.id === action.payload);
      if (todo) {
        todo.completed = !todo.completed;
      }
    },
    deleteTodo: (state, action: PayloadAction<number>) => {
      state.todos = state.todos.filter(todo => todo.id !== action.payload);
    },
  },
});

export const selectTodos = (state: RootState) => state.todos.todos;
export const { addTodo, toggleTodo, deleteTodo } = todoSlice.actions;
export const todoReducer = todoSlice.reducer;
