import { useAppDispatch, useAppSelector } from '../../common/app/constants/store';
import React from 'react';
import { selectNested, toggle } from '../reducers/nested.reducer';
import { Button } from '@mui/material';

export const TEXT = {
  ON: 'ON',
  OFF: 'OFF',
};

export function Nested() {
  const { isOn } = useAppSelector(selectNested);
  const dispatch = useAppDispatch();

  return (
    <div>
      <Button onClick={() => dispatch(toggle())}>is {isOn ? TEXT.ON : TEXT.OFF}</Button>
    </div>
  );
}
