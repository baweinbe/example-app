import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import { renderComponent } from "../../test/test.util";
import { Nested, TEXT } from "./Nested";

it('renders OFF and toggles to ON', async () => {
    const { user } = renderComponent(<Nested />);
    const button = screen.getByRole('button');
    expect(button.textContent).toContain(TEXT.OFF);
    user.click(button);
    await waitFor(() => {
        expect(button.textContent).toContain(TEXT.ON);
    });
});