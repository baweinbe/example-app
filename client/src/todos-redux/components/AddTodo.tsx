import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addTodo } from '../reducers/todos.reducer';

export const AddTodoForm = () => {
  const dispatch = useDispatch();
  const [text, setText] = useState('');

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    dispatch(
      addTodo({
        id: Date.now(),
        text,
        completed: false,
      }),
    );
    setText('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" value={text} onChange={event => setText(event.target.value)} />
      <button type="submit">Add Todo</button>
    </form>
  );
};
