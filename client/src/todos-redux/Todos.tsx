import React from 'react';
import { deleteTodo, selectTodos, toggleTodo } from './reducers/todos.reducer';
import { AddTodoForm } from './components/AddTodo';
import { useAppDispatch, useAppSelector } from '../common/app/constants/store';
import { Nested } from './components/Nested';

export const Todos = () => {
  const todos = useAppSelector(selectTodos);
  const dispatch = useAppDispatch();

  // console.log('Todos');

  return (
    <div>
      <Nested />
      <AddTodoForm />
      <ul>
        {todos.map(todo => (
          <li key={todo.id}>
            <input type="checkbox" checked={todo.completed} onChange={() => dispatch(toggleTodo(todo.id))} />
            {todo.text}
            <button onClick={() => dispatch(deleteTodo(todo.id))}>Delete</button>
          </li>
        ))}
      </ul>
      <Nested />
    </div>
  );
};
