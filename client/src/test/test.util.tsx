import { render } from '@testing-library/react';
import { ReactNode } from "react";
import { store } from "../common/app/constants/store";
import { Provider } from "react-redux";
import userEvent from '@testing-library/user-event';

export function renderComponent(component: ReactNode) {
    return {
        user: userEvent.setup(),
        ...render(<Provider store={store}>{component}</Provider>),
    };
}