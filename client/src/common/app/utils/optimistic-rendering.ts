import { useQueryClient } from '@tanstack/react-query';
import { QueryKey } from '@tanstack/query-core/src/types';

//TODO: type better
type CreateOptimisticData = (newData: { data: any }, old: any) => void;

type OptimisticOptions = {
  queryKey: QueryKey;
  createOptimisticData: CreateOptimisticData;
};

const OPTIMISTIC_PREFIX = 'optimistic';

export function optimisticId() {
  return `${OPTIMISTIC_PREFIX}-${Date.now()}`;
}

export function isOptimisticId(id: string) {
  return id.startsWith(OPTIMISTIC_PREFIX);
}

export function createListItem(): CreateOptimisticData {
  return (newData, old) => [
    ...old,
    {
      ...newData.data,
      id: optimisticId(),
    },
  ];
}

export function updateListItem<D extends { id: string }>(id: string): CreateOptimisticData {
  return (newData, old) => {
    return old.map((t: D) => {
      if (t.id === id) {
        return {
          ...t,
          ...newData.data,
        };
      }
      return t;
    });
  };
}

export function deleteListItem<D extends { id: string }>(id: string): CreateOptimisticData {
  return (newData, old) => old.filter((t: D) => t.id !== id);
}

export function useOptimisticOptions({ queryKey, createOptimisticData }: OptimisticOptions) {
  const queryClient = useQueryClient();
  return {
    onMutate: async (newData: any) => {
      // Cancel any outgoing re-fetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries({ queryKey });

      // Snapshot the previous value
      const previous = queryClient.getQueryData(queryKey);

      // Optimistically update to the new value
      queryClient.setQueryData(queryKey, old => createOptimisticData(newData, old));

      // Return a context object with the snapshot value
      return { previous };
    },
    // If the mutation fails, use the context returned from onMutate to roll back
    onError: (err: Error, newData: any, context: any) => {
      queryClient.setQueryData(queryKey, context?.previous);
    },
    // Always re-fetch after error or success:
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey });
    },
  };
}
