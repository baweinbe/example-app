export const TODO_SERVER_PATH = '/todos-server';
export const TODO_REDUX_PATH = '/todos-redux';
export const TODO_MOBX_PATH = '/todos-mobx';
export const FAQ_PATH = '/faq';
export const MODAL_PATH = '/modal';
export const STATE_MACHINE_PATH = '/state-machine';
export const STATE_MACHINE_CHILD_PATH = '/state-machine/child';
