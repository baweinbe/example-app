import { QueryClient } from '@tanstack/react-query';

export const apiClient = new QueryClient({
  defaultOptions: {
    mutations: {
      useErrorBoundary: true,
    },
    queries: {
      useErrorBoundary: true,
    },
  },
});
