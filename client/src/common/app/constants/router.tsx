import { createBrowserRouter } from 'react-router-dom';
import { TodosServer } from '../../../todos-server/TodosServer';
import { ErrorPage } from '../components/ErrorPage';
import { Todos } from '../../../todos-redux/Todos';
import React from 'react';
import { Faq } from '../../../faq/Faq';
import { Nav } from '../components/Nav';
import {
  FAQ_PATH,
  MODAL_PATH,
  STATE_MACHINE_CHILD_PATH,
  STATE_MACHINE_PATH,
  TODO_MOBX_PATH,
  TODO_REDUX_PATH,
  TODO_SERVER_PATH
} from './paths';
import { TodosMobx } from '../../../todos-mobx/TodosMobx';
import {ExampleModalPage} from "../../../example-modal/ExampleModalPage";
import {Initial} from "../../../state-machine/Initial";
import {StateMachine} from "../../../state-machine/components/StateMachine";

export const router = createBrowserRouter([
  {
    path: '/',
    element: <Nav />,
    children: [
      {
        path: TODO_SERVER_PATH,
        element: <TodosServer />,
        errorElement: <ErrorPage />,
      },
      {
        path: TODO_REDUX_PATH,
        element: <Todos />,
        errorElement: <ErrorPage />,
      },
      {
        path: TODO_MOBX_PATH,
        element: <TodosMobx />,
        errorElement: <ErrorPage />,
      },
      {
        path: FAQ_PATH,
        element: <Faq />,
        errorElement: <ErrorPage />,
      },
      {
        path: MODAL_PATH,
        element: <ExampleModalPage />,
        errorElement: <ErrorPage />,
      },
      {
        path: STATE_MACHINE_PATH,
        element: <Initial />,
        errorElement: <ErrorPage />,
      },
      {
        path: STATE_MACHINE_CHILD_PATH,
        element: <StateMachine />,
        errorElement: <ErrorPage />,
      }
    ],
  },
]);
