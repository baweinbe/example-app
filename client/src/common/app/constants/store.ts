import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { todoReducer } from '../../../todos-redux/reducers/todos.reducer';
import { nestedReducer } from '../../../todos-redux/reducers/nested.reducer';
import {machineReducer} from "../../../state-machine/reducers/state-machine.reducer";

export const store = configureStore({
  reducer: {
    todos: todoReducer,
    nested: nestedReducer,
    machine: machineReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
