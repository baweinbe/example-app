import { Link, Outlet, useLocation } from 'react-router-dom';
import React, { useEffect } from 'react';
import {
  FAQ_PATH,
  MODAL_PATH,
  STATE_MACHINE_PATH,
  TODO_MOBX_PATH,
  TODO_REDUX_PATH,
  TODO_SERVER_PATH
} from '../constants/paths';

export function Nav() {
  // const location = useLocation();
  //
  // useEffect(() => {
  //     console.log(location);
  // }, [location]);

  return (
    <div>
      <ul>
        <li>
          <Link to={TODO_SERVER_PATH}>Tan Stack</Link>
        </li>
        <li>
          <Link to={TODO_REDUX_PATH}>Redux</Link>
        </li>
        <li>
          <Link to={TODO_MOBX_PATH}>Mobx</Link>
        </li>
        <li>
          <Link to={FAQ_PATH}>FAQ</Link>
        </li>
        <li>
          <Link to={MODAL_PATH}>Modal</Link>
        </li>
        <li>
          <Link to={STATE_MACHINE_PATH}>State Machine</Link>
        </li>
      </ul>
      <Outlet />
    </div>
  );
}
