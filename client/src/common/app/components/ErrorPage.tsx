import { useRouteError } from 'react-router-dom';
import React from 'react';

export function ErrorPage() {
  const error = useRouteError() as Error;
  console.error(error);

  return (
    <div>
      <p>Sorry, an unexpected error has occurred.</p>
      <p>
        <i>{error.message}</i>
      </p>
    </div>
  );
}
