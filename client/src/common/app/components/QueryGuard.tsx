export const QueryGuard = ({ isLoading, children }: any) => {
  if (isLoading) {
    return 'Loading...';
  }

  return children;
};
