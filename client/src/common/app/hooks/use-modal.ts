import { useSearchParams } from 'react-router-dom';
import { useMemo } from 'react';

export function useModal(queryParamValue: string, queryParamKey = 'm') {
  const [searchParams, setSearchParams] = useSearchParams();

  const withoutParam = useMemo(() => {
    const obj = Object.fromEntries(searchParams.entries());
    delete obj[queryParamKey];
    return obj;
  }, [searchParams]);

  const withParam = useMemo(() => {
    const obj = Object.fromEntries(searchParams.entries());
    obj[queryParamKey] = queryParamValue;
    return obj;
  }, [searchParams]);

  return {
    isOpen: searchParams.get(queryParamKey) === queryParamValue,
    onOpen: () => setSearchParams(withParam),
    onClose: () => setSearchParams(withoutParam),
  };
}
