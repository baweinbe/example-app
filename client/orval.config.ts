import { defineConfig } from 'orval';

export default defineConfig({
  accountsV1: {
    input: '../server/api.yml',
    output: {
      mode: 'split',
      target: './src/generated/api.ts',
      client: 'react-query',
      mock: true,
      override: {
        mutator: {
          path: './src/common/app/constants/http-client.ts',
          name: 'httpClient',
        },
      },
    },
    hooks: {
      afterAllFilesWrite: 'prettier --write',
    },
  },
});
